;;
;; rob-dark-theme.el - rob@pyrite.org personal color theme for emacs
;;

(deftheme rob-dark
  "Personal theme for rob@pyrite.org.")

(let ((class '((class color) (min-colors 89)))
      (background-color "#000000")
      (border-color "#1a1a1a")
      (cursor-color "#fce94f")
      (foreground-color "#ebebeb")
      (mouse-color "black")
      (blue "#b0d7e8")
      (blue-1 "#a1c3e8")
      (blue-2 "#92b3e8")
      (gray "#878787")
      (purple "#ff73fd")
      (purple-1 "#ffccff")
      (purple-2 "#d4caed")
      (green "#b0e3b5")
      (yellow "#e9d99f")
      (yellow-1 "#dae085")
      (red "#c42149")
      (magenta "#ff5996")
      (peach "#e5786d")
      )

  (custom-theme-set-faces
   'rob-dark
   ;; Default and cursor
   `(default ((,class (:foreground ,foreground-color :background ,background-color))))
   ;; Highlighting
   `(fringe ((,class (:background "#1a1a1a"))))
   `(highlight ((,class (:background "#235251"))))
   `(region ((,class (:background "#235251"))))
   `(show-paren-match-face ((,class (:background "#857b6f"))))
   `(tool-tips ((,class (:inherit 'variable-pitch
                                  :foreground "black" :background "lightyellow"))))
   `(tooltip ((,class (:inherit 'variable-pitch
                                :foreground "black" :background "lightyellow"))))
   `(bold ((,class (:bold t :underline nil :background nil))))
   `(italic ((,class (:italic t :underline nil :background nil))))
   ;; Prompts
   `(minibuffer-prompt ((,class (:foreground "#cee0f3" :bold t))))
   ;; Mode Line
   `(mode-line ((,class
                 (:inherit 'variable-pitch
                           :box (:line-width 1 :style pressed-button)
                           :foreground "#b0d7e8" :background "#000000"))))
   `(mode-line-inactive ((,class
                          (:inherit 'variable-pitch
                                    :box (:line-width 1 :style pressed-button)
                                    :foreground "#606060" :background "#000000"))))

   ;; Font-lock
   `(font-lock-builtin-face ((,class (:foreground ,blue))))
   `(font-lock-comment-face ((,class (:foreground ,gray))))
   `(font-lock-constant-face ((,class (:foreground ,peach))))
   `(font-lock-doc-face ((,class (:foreground ,green))))
   `(font-lock-function-name-face ((,class (:foreground ,magenta))))
   `(font-lock-keyword-face ((,class (:foreground ,blue-1))))
   `(font-lock-preprocessor-face ((,class (:foreground ,blue-2))))
   `(font-lock-reference-face ((,class (:foreground ,purple-1))))
   `(font-lock-string-face ((,class (:foreground ,green))))
   `(font-lock-type-face ((,class (:foreground ,yellow))))
   `(font-lock-variable-name-face ((,class (:foreground ,purple-2))))
   `(font-lock-warning-face ((,class (:foreground "#c42149" :bold t))))
   ;; Org-mode
   `(org-hide ((,class (:foreground "#000000"))))
   `(org-level-1 ((,class (:foreground ,yellow :height 1.0))))
   `(org-level-2 ((,class (:foreground ,purple-2 :height 1.0))))
   `(org-level-3 ((,class (:foreground ,blue :height 1.0))))
   `(org-level-4 ((,class (:foreground ,blue-1))))
   `(org-level-5 ((,class (:foreground ,green))))
   `(org-level-6 ((,class (:foreground ,peach))))
   `(org-todo ((,class (:bold t :foreground "#cc2626"))))
   `(org-done ((,class (:bold t :foreground "#66ee66"))))
   `(org-block ((,class (:foreground "#bbbbbc"))))
   `(org-quote ((,class (:inherit org-block :slant italic))))
   `(org-verse ((,class (:inherit org-block :slant italic))))
   ;; Cluck
   `(cluck-pltish-comment-face ((,class (:inherit font-lock-comment-face))))
   `(cluck-pltish-paren-face ((,class (:foreground "#f0f0f0" :bold t))))
   `(cluck-pltish-selfeval-face ((,class (:inherit font-lock-constant-face))))
   `(cluck-pltish-colon-keyword-face ((,class (:inherit font-lock-preprocessor-face))))
   `(cluck-pltish-defn-face ((,class (:inherit font-lock-function-name-face))))
   `(cluck-pltish-module-defn-face ((,class (:inherit font-lock-type-face))))
   `(cluck-pltish-keyword-face ((,class (:inherit font-lock-keyword-face))))
   `(cluck-threesemi-semi-face ((,class (:inherit font-lock-doc-face))))
   `(cluck-threesemi-text-face ((,class (:inherit font-lock-doc-face))))
   
   )
  
)

(provide-theme 'rob-dark)
