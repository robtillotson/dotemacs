# -*-Ruby-*-

require 'rake'

task :default => [:build]

RakeFileUtils.verbose(false)

if RUBY_PLATFORM =~ /darwin/
  EMACS = "/Applications/Emacs.app/Contents/MacOS/Emacs"
else
  EMACS = "emacs"
end

INIT_LOADPATH = "-l #{File.join(Dir.pwd,'init-loadpath.el')}"

def byte_compile_dir(path)
  puts "byte-compiling #{path}"
  sh "#{EMACS} #{INIT_LOADPATH} --batch --eval '(byte-recompile-directory \"#{path}\" 0)' >>build.log 2>&1"
end

def emacs_make(path, arg="")
  puts "building #{path} #{arg}"
  sh "cd #{path}; make EMACS=#{EMACS} #{arg} >>build.log 2>&1"
end

task :build do
  sh "rm -f build.log"
  
  # First, pull the submodules, just in case
  puts "Pulling submodules..."
  sh "git submodule update --init"
  
  # *** Vendor packages
  # CEDET
  #puts "building vendor/cedet"
  #sh "cd vendor/cedet; #{EMACS} --batch --no-site-file -l cedet-build.el -f cedet-build >>build.log 2>&1"

  # Org-mode
  #emacs_make "vendor/org-mode"
  #emacs_make "vendor/org-mode", "info"

  # ECB
  emacs_make "vendor/ecb", "'CEDET=' all"
  sh "cd vendor/ecb; #{EMACS} #{INIT_LOADPATH} --batch -l ecb-autogen.el -f ecb-update-autoloads >>build.log 2>&1"
  
  # The rest
  FileList.new("vendor/*").exclude("cedet","ecb","org-mode","bookmark+","nxhtml").each do |file|
    if File.directory?(file)
      byte_compile_dir file
    end
  end

  #puts "byte-compiling vendor/nxhtml"
  #sh "cd vendor/nxhtml; #{EMACS} --batch -l autostart.el -l nxhtml-web-vcs.el -l nxhtmlmaint.el -f nxhtmlmaint-byte-compile-all >>build.log 2>&1"
  
  # Bookmark+ needs special handling because it uses a macro from bookmark.el
  # which must be loaded before compiling.
  puts "byte-compiling vendor/bookmark+"
  sh "#{EMACS} #{INIT_LOADPATH} --batch --eval '(progn (load-library \"bookmark\") (byte-recompile-directory \"vendor/bookmark+\" 0))' >>build.log 2>&1"
  
  puts "byte-compiling vendor/*.el"
  sh "cd vendor; #{EMACS} #{INIT_LOADPATH} --batch -f batch-byte-compile *.el >>build.log 2>&1"
      
  #byte_compile_dir "elpa"
end
