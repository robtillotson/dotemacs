
;(defvar *emacs-load-start* (current-time))

;;;; Foundation

;; (defadvice load (around debug-loading)
;;   (message "Loading %s..." (ad-get-arg 0))
;;   ad-do-it
;;   (message "Loaded %s." (ad-get-arg 0)))
;; (ad-activate 'load)

;;; Code:

(require 'cl)

(defmacro after (mode &rest body)
  "`eval-after-load' MODE evaluate BODY."
  (declare (indent 1) (debug t))
  `(eval-after-load (symbol-name ',mode)
     '(progn ,@body)))

;; While "after" is the basic eval-after-load macro, the following
;; macros are syntactic sugar to make package configuration a little
;; more declarative.
;;
;; (require-package foo) - if foo is installed, "require"s it and executes body after load.
;; (with-package foo) - executes body after package is installed.
;; (when-package-installed foo) - executes body only if package is installed
;;
;; If I ever decide not to use pallet/carton to manage packages, these could be
;; augmented to automatically select packages for installation etc.

(defmacro require-package (sym &rest body)
  (declare (indent 1) (debug t))
  (let ((autoloads (intern (concat (symbol-name sym) "-autoloads"))))
    `(progn
       (eval-after-load ',autoloads
         '(require ',sym))
       (eval-after-load ',sym
         '(progn ,@body)))))

(defmacro with-package (package &rest body)
  (declare (indent 1) (debug t))
  `(eval-after-load ',package
     '(progn ,@body)))

;; A version of "after" that makes a little clearer what
;; loading after foo-autoloads actually means.
(defmacro when-package-installed (pkg &rest body)
  (declare (indent 1) (debug t))
  (let ((autoloads (intern (concat (symbol-name sym) "-autoloads"))))
    `(eval-after-load ',autoloads
       '(progn ,@body))))

;; Declare a development package that we are working on ourselves.
;; If it has a source directory in ~/src/... add that to the load
;; path and immediately execute the body.  Otherwise, if the
;; package is installed execute the body on an eval-after-load of
;; the autoloads.
(defmacro with-dev-package (sym &rest body)
  (declare (indent 1) (debug t))
  (let ((path (expand-file-name (concat "~/src/" (symbol-name sym)))))
    `(cond ((file-directory-p ,path)
            (add-to-list 'load-path ,path)
            (progn ,@body))
           ((package-installed-p ',sym)
            (eval-after-load (symbol-name ',sym)
              '(progn ,@body))))))

;; Like with-dev-package but immediately requires the package too.
(defmacro with-dev-package* (sym &rest body)
  (declare (indent 1) (debug t))
  `(progn
     (with-dev-package ,sym)
     (require ',sym)))

(font-lock-add-keywords 'emacs-lisp-mode
  '(("(\\<\\(after\\)\\> +(?\\([^()]+\\))?"
     (1 'font-lock-keyword-face)
     (2 'font-lock-constant-face))
    ("(\\<\\(with-dev-package\\*?\\)\\> +(?\\([^()]+\\))?"
     (1 'font-lock-keyword-face)
     (2 'font-lock-constant-face))))

(defun imenu-elisp-sections ()
  (setq imenu-prev-index-position-function nil)
  (add-to-list 'imenu-generic-expression '("Sections" "^;;;; \\(.+\\)$" 1) t))

(add-hook 'emacs-lisp-mode-hook 'imenu-elisp-sections)

;;;; Load-path
;;(load-file (expand-file-name "~/.emacs.d/init-loadpath.el"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d"))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/vendor"))

;;;; Custom
(setq custom-file (expand-file-name "~/.emacs.d/custom.el"))
(load custom-file)

;;;; Basic settings
;;
;;
(defvar *hostname*
  (let ((n (system-name))) (substring n 0 (string-match "\\." n))))

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message "rob"
      visible-bell nil
      ring-bell-function 'ignore
      eval-expression-debug-on-error t
      dropbox-directory "~/Dropbox"
      user-mail-address "rob@pyrite.org"
      user-full-name "Rob Tillotson"
      calendar-latitude [33 40 north]
      calendar-longitude [86 23 west]
      calendar-location-name "Odenville, AL"
      )

;;;; Backup and autosave
;;
(let ((temp-file-dir (concat temporary-file-directory user-login-name "/")))
  (make-directory temp-file-dir t)
  (setq backup-by-copying t
        backup-directory-alist `(("." . ,temp-file-dir)
                                 (,tramp-file-name-regexp nil))
        auto-save-list-file-prefix (concat temp-file-dir ".auto-saves-")
        auto-save-file-name-transforms `((".*" ,temp-file-dir t))
        )
  (global-auto-revert-mode t))

;; Some basic tweaks
(fset 'yes-or-no-p 'y-or-n-p)

;; For some reason, debug-on-error is on by default in one of
;; the emacsen I use.  That's kind of annoying.
(add-hook 'after-init-hook
          (lambda () (setq debug-on-error nil)))
(setq eval-expression-debug-on-error t)

;;;; User Interface
;;
(setq font-lock-verbose nil)

;;----------------------------------------------------------------------------
;; Include buffer name and file path in title bar
;;----------------------------------------------------------------------------
(defun concise-buffer-file-name ()
  (when (buffer-file-name)
    (replace-regexp-in-string (regexp-quote (getenv "HOME")) "~" (buffer-file-name))))
(setq frame-title-format '("%b - " *user* "@" *hostname*
                           " - "
                           (:eval (concise-buffer-file-name))))


;;----------------------------------------------------------------------------
;; Show a marker in the left fringe for lines not in the buffer
;;----------------------------------------------------------------------------
(setq indicate-empty-lines t)


;;----------------------------------------------------------------------------
;; Modeline tweaks
;;----------------------------------------------------------------------------
(size-indication-mode)

;;----------------------------------------------------------------------------
;; Window size and features
;;----------------------------------------------------------------------------
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))


(defun rob-x11-maximize (&optional f)
       (interactive)
       (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
	    		 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
       (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
	    		 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))

(defun rob-set-fonts (fixed &optional variable modeline)
  (set-default-font fixed)
  (set-face-font 'fixed-pitch fixed)
  (add-to-list 'default-frame-alist `(font . ,fixed))
  (when variable
    (set-face-font 'variable-pitch variable))
  (when modeline
    (set-face-font 'mode-line modeline)))

;;; Set up GUI:
;;
(let ((hostname (system-name)))
  (if window-system
      (progn
         (menu-bar-mode 1)
         (mouse-wheel-mode t)
         (cond
               ;; Windows X11 server (xming et. al.)
               ((member (x-server-vendor) '("HC-Consult" "Colin Harrison"))
                (add-to-list 'default-frame-alist '(width . 194))
                (add-to-list 'default-frame-alist '(height . 50))
                (menu-bar-mode 1)
                ; Ubuntu Mono-12 Ubuntu-12
                (rob-set-fonts "Pragmata Pro-12" "Calibri-14" "Ubuntu-12"))
               ;; Host specific settings
               ((string-match "^myka" hostname)
                (rob-set-fonts "Pragmata Pro-12" "Calibri-14" "Ubuntu-12"))
               ((string-match "^astrid" hostname)
                (rob-set-fonts "Pragmata Pro-11" "Calibri-13" "Ubuntu-12"))
               ;; Generic linux X server
               ((eq system-type 'gnu/linux)
                (rob-set-fonts "Ubuntu Mono-12" "Ubuntu-12")
                ;; (set-default-font "-*-Ubuntu Mono-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1")
                ;; (set-frame-font "-*-Ubuntu Mono-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1")
                )
               ;; Windows
               ((or (eq system-type 'windows-nt)
                    (eq window-system 'w32))
                (set-default-font "-outline-Consolas-normal-normal-normal-mono-13-*-*-*-c-*-iso8859-1")
                (add-to-list 'default-frame-alist '(alpha . (97 80)))))
         (when (eq window-system 'x)
           (rob-x11-maximize)))
    ;; No GUI
    (menu-bar-mode 0)))


;;;; Packaging system
;;
;;
(defun rob/find-installed-cask-package ()
  "Locate a currently installed version of cask.el, from the cask-installed elpa dir."
  (let* ((cask-elpa-path (format "%s/.cask/%s/elpa" user-emacs-directory emacs-version))
         (cask-paths 
          (delq nil (mapcar (lambda (x) (and (> (length x) 5)
                                             (string= "cask-" (substring x 0 5))
                                             x))
                            (directory-files cask-elpa-path)))))
    (when cask-paths
      (expand-file-name (format "%s/%s/cask.el" cask-elpa-path (car cask-paths))))
    ))

;(require 'cask (rob/find-installed-cask-package))
(require 'cask "~/.cask/cask.el")
(cask-initialize)

(require 'package)
;(require 'package-helper)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
;(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;(add-to-list 'package-archives '("marmalade". "http://marmalade-repo.org/packages/"))
;(add-to-list 'package-archives '("elpa" . "http://tromey.com/elpa/"))
;(require 'use-package)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(after pallet-autoloads
  (require 'pallet))

(require 'use-package)
;;;; Evil
;;
;;
(use-package undo-tree
  :diminish (undo-tree-mode . "UT"))

(after evil-autoloads
  (setq evil-find-skip-newlines t)
  (setq evil-move-cursor-back nil)
  (setq evil-cross-lines t)
  (setq evil-leader/leader ",")
  (setq evil-leader/in-all-states t)

  (require 'evil-leader)
  (require 'evil)

  ;; Modified version of evil-ret that moves the cursor to the first
  ;; non-blank on the line, like vim.
  (evil-define-motion rob/evil-ret (count)
    "Move the cursor COUNT lines down.
    If point is on a widget or a button, click on it.
    In Insert state, insert a newline."
    :type line
    (let* ((field  (get-char-property (point) 'field))
           (button (get-char-property (point) 'button))
           (doc    (get-char-property (point) 'widget-doc))
           (widget (or field button doc)))
      (cond
       ((and widget
             (fboundp 'widget-type)
             (fboundp 'widget-button-press)
             (or (and (symbolp widget)
                      (get widget 'widget-type))
                 (and (consp widget)
                      (get (widget-type widget) 'widget-type))))
        (when (evil-operator-state-p)
          (setq evil-inhibit-operator t))
        (when (fboundp 'widget-button-press)
          (widget-button-press (point))))
       ((and (fboundp 'button-at)
             (fboundp 'push-button)
             (button-at (point)))
        (when (evil-operator-state-p)
          (setq evil-inhibit-operator t))
        (push-button))
       ((or (evil-emacs-state-p)
            (and (evil-insert-state-p)
                 (not buffer-read-only)))
        (if (not evil-auto-indent)
            (newline count)
          (delete-horizontal-space t)
          (newline count)
          (indent-according-to-mode)))
       (t
        (evil-next-line count)
        (evil-first-non-blank)))))

  (define-key evil-motion-state-map (kbd "RET") 'rob/evil-ret)

  (define-key evil-motion-state-map "j" #'evil-next-visual-line)
  (define-key evil-motion-state-map "k" #'evil-previous-visual-line)
  (define-key evil-motion-state-map "$" #'evil-end-of-visual-line)
  (define-key evil-motion-state-map "^" #'evil-first-non-blank-of-visual-line)
  (define-key evil-motion-state-map "0" #'evil-beginning-of-visual-line)

  (define-key evil-normal-state-map (kbd "M-.") 'find-tag)
  
  (setq evil-mode-line-format nil)

  (loop for (mode . state) in '((inferior-emacs-lisp-mode . emacs)
                                (comint-mode . emacs)
                                (shell-mode . emacs)
                                (term-mode . emacs)
                                (magit-branch-manager-mode-map . emacs))
        do (evil-set-initial-state mode state))

  (evil-define-key 'normal lisp-interaction-mode-map (kbd "C-j") 'eval-print-last-sexp)

  (evil-define-key 'insert lisp-interaction-mode-map (kbd "C-j") 'eval-print-last-sexp)

  (after god-mode-autoloads
    ;;; Thanks to https://gist.github.com/gridaphobe/9765143
    (require 'god-mode)

    (evil-define-state god
      "God state."
      :tag (propertize " G " 'face '((:background "green")))
      :message "-- GOD MODE --"
      :entry-hook (evil-god-start-hook)
      :exit-hook (evil-god-stop-hook)
      :input-method t
      :intercept-esc nil)

    (defun evil-god-start-hook ()
      (diminish 'god-local-mode)
      (god-local-mode 1))

    (defun evil-god-stop-hook ()
      (god-local-mode -1)
      (diminish-undo 'god-local-mode))

    (defvar evil-execute-in-god-state-buffer nil)

    (defun evil-stop-execute-in-god-state ()
      (when (and (not (eq this-command #'evil-execute-in-god-state))
                 (not (minibufferp)))
        (remove-hook 'post-command-hook 'evil-stop-execute-in-god-state)
        (when (buffer-live-p evil-execute-in-god-state-buffer)
          (with-current-buffer evil-execute-in-god-state-buffer
            (if (and (eq evil-previous-state 'visual)
                     (not (use-region-p)))
                (progn
                  (evil-change-to-previous-state)
                  (evil-exit-visual-state))
              (evil-change-to-previous-state))))
        (setq evil-execute-in-god-state-buffer nil)))

    (evil-define-command evil-execute-in-god-state ()
      "Execute the next command in God state."
      (add-hook 'post-command-hook #'evil-stop-execute-in-god-state t)
      (setq evil-execute-in-god-state-buffer (current-buffer))
      (cond
       ((evil-visual-state-p)
        (let ((mrk (mark))
              (pnt (point)))
          (evil-god-state)
          (set-mar mrk)
          (goto-char pnt)))
       (t
        (evil-god-state)))
      (evil-echo "Switched to God state for the next command ..."))

    (evil-define-key 'normal global-map "," 'evil-execute-in-god-state))

  (after paredit
    (require 'evil-paredit))
  (evil-mode 1))

;; These tags are meant to look more or less like the ones used in powerline for vim
(setq evil-normal-state-tag   "  N  "
      evil-emacs-state-tag    (propertize " EMACS " 'face '((:background "orange" :foreground "black")))
      evil-insert-state-tag   (propertize " INSERT " 'face '((:background "grey80" :foreground "black")))
      evil-motion-state-tag   (propertize " MOTION " 'face '((:background "blue")))
      evil-visual-state-tag   (propertize " VISUAL " 'face '((:background "red")))
      evil-operator-state-tag (propertize " OPER " 'face '((:background "purple"))))

;;;; Misc
;;
;;

;; Color theme code just prior to switching to deftheme
(require 'rob-dark)
(rob-dark)
(setq color-theme-load-all-themes nil)
(setq color-theme-libraries '())
(add-hook 'after-make-frame-functions
          (lambda (frame)
            (set-variable 'color-theme-is-global nil)
            (select-frame frame)
            (if window-system
                (rob-dark)
              (rob-dark-tty))))

;; diff-hl
(use-package diff-hl
  :init (progn 
          (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
          (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)))

;; yasnippet
(use-package yasnippet
  :init (progn
          (yas-global-mode 1)
          (setq yas/prompt-functions '(yas/ido-prompt yas/completing-prompt))))

;; multi-term
(setq multi-term-program "/bin/zsh")
(setq multi-term-dedicated-select-after-open-p t)

;; markdown-mode
(use-package markdown-mode
  :mode (("\\.\\(md\\|mdown\\|markdown\\)\\'" . markdown-mode)
         ("\\.txt\\'" . markdown-mode))
  :init (progn
          (add-hook 'markdown-mode-hook 'outline-minor-mode)))

;; auto-complete
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/el-get/auto-complete/dict")
(setq ac-auto-start 4)
(ac-config-default)

;; rinari
(after rinari-autoloads
  (require 'rinari)
  (setq rinari-tags-file-name "TAGS")
  (add-hook 'ruby-mode-hook
            (lambda () (define-key ruby-mode-map "\C-m" 'reindent-then-newline-and-indent))))

;; Scala
(after ensime-autoloads
  (require 'ensime)
  (add-hook 'scala-mode-hook 'ensime-scala-mode-hook))

;; Python
(with-dev-package journyx-dev-el
  (require 'journyx-dev)
  (add-hook 'python-mode-hook 'journyx-development-mode-maybe))

;; Flycheck
(use-package flycheck
  :init (progn
          (add-hook 'after-init-hook #'global-flycheck-mode)))

;; deft
(use-package deft
  :bind ("\C-cd" . deft)
  :init (progn
          (setq deft-text-mode 'markdown-mode
                deft-extension "txt"
                deft-directory "~/Dropbox/Notes"
                deft-use-filename-as-title t)))

;; geben (remote debugging)
(after geben
  (evil-set-initial-state 'geben-mode 'emacs)
  (evil-set-initial-state 'geben-context-mode 'emacs)
  (evil-set-initial-state 'geben-breakpoint-list-mode 'emacs)
  (evil-set-initial-state 'geben-backtrace-mode 'emacs))

(after smartparens-autoloads
  (require 'smartparens-config)
  (smartparens-global-mode t)
  (show-smartparens-global-mode t)
  (sp-use-smartparens-bindings))

(use-package paredit
  :diminish (paredit-mode . "Par")
  :init (progn
          (require 'paredit-menu)
          (dolist (hook (list 'emacs-lisp-mode-hook
                              'ielm-mode-hook
                              'lisp-mode-hook
                              'inferior-lisp-mode-hook
                              'slime-repl-mode-hook
                              'scheme-mode-hook
                              'inferior-scheme-mode-hook))
            (add-hook hook 'enable-paredit-mode))
          (bind-key (kbd "RET") 'paredit-newline emacs-lisp-mode-map)
          (bind-key (kbd "C-j") 'eval-print-last-sexp lisp-interaction-mode-map)))

;; bookmark+
(use-package bookmark+
  :init (progn
          (require 'ffap)
          (setq bookmark-save-flag 1)))


(defun powerline-rob-theme ()
  "Setup a default mode-line."
  (interactive)
  (setq-default mode-line-format
                '("%e"
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          (face1 (if active 'powerline-active1
                                   'powerline-inactive1))
                          (face2 (if active 'powerline-active2
                                   'powerline-inactive2))
                          (separator-left
                           (intern (format "powerline-%s-%s"
                                           powerline-default-separator
                                           (car powerline-default-separator-dir))))
                          (separator-right
                           (intern (format "powerline-%s-%s"
                                           powerline-default-separator
                                           (cdr powerline-default-separator-dir))))
                          (lhs (list
                                (powerline-raw evil-mode-line-tag nil 'l)
                                (powerline-raw "%*" nil 'l)
                                (powerline-buffer-size nil 'l)

                                (powerline-raw mode-line-mule-info nil 'l)
                                (powerline-buffer-id nil 'l)

                                (when (and (boundp 'which-func-mode) which-func-mode)
                                  (powerline-raw which-func-format nil 'l))

                                (powerline-raw " ")
                                (funcall separator-left mode-line face2)

                                (when (boundp 'erc-modified-channels-object)
                                  (powerline-raw erc-modified-channels-object
                                                 face1 'l))

                                (powerline-major-mode face2 'l)
                                (powerline-process face2)
                                (powerline-raw " " face2)
                                (funcall separator-left face2 face1)
                                (powerline-minor-modes face1 'l)
                                (powerline-narrow face1 'l)

                                (powerline-raw " " face1)
                                (funcall separator-left face1 face2)

                                (powerline-vc face2 'r)))
                          (rhs (list
                                (powerline-raw global-mode-string face2 'r)

                                (funcall separator-right face2 face1)

                                (powerline-raw "%4l" face1 'l)
                                (powerline-raw ":" face1 'l)
                                (powerline-raw "%3c" face1 'r)

                                (funcall separator-right face1 mode-line)
                                (powerline-raw " ")

                                (powerline-raw "%6p" nil 'r)

                                (powerline-hud face2 face1))))
                     (concat
                      (powerline-render lhs)
                      (powerline-fill face2 (powerline-width rhs))
                      (powerline-render rhs)))))))

(use-package powerline
  :init (progn
          (powerline-rob-theme)))


;;
;; Smart-mode-line
;;
(after smart-mode-line-autoloads
  (setq evil-mode-line-format 'before)
  (setq evil-normal-state-tag   "  N  "
        evil-emacs-state-tag    (propertize "  Em  " 'face '((:foreground "orange")))
        evil-insert-state-tag   (propertize "  Ins  " 'face '((:foreground "grey80")))
        evil-motion-state-tag   (propertize "  Mot  " 'face '((:foreground "blue")))
        evil-visual-state-tag   (propertize "  Vis  " 'face '((:foreground "red")))
        evil-operator-state-tag (propertize "  Op  " 'face '((:foreground "purple"))))
  (setq sml/vc-mode-show-backend t)

  (setq sml/replacer-regexp-list
        '(("^~/Dropbox/" ":DB:")
          ("^~/\\.emacs\\.d/" ":Emacs:")
          ("^~/jx/s/\\(.*?\\)/wtp/wtlib/jx_api/" ":jxAPI/\\1:")
          ("^~/jx/s/\\(.*?\\)/wtp/\\(.*?\\)/" ":JX/\\1/\\2:")
          ("^~/jx/s/\\(.*?\\)/" ":JX/\\1:")
          ))

  (add-hook 'after-init-hook 'sml/setup))

;;;; Tools
;;
;;
;;
(setq epa-file-cache-passphrase-for-symmetric-encryption t)
;;
(iswitchb-mode 1)
;;
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Midnight mode - automatically clear unused buffers every night
(require 'midnight)
(midnight-delay-set 'midnight-delay "6:00am")

;;
;; eshell
;;
(require 'eshell)
(require 'em-smart)
(setq eshell-where-to-jump 'begin)
(setq eshell-review-quick-commands nil)
(setq eshell-smart-space-goes-to-end t)
(setq eshell-directory-name (expand-file-name "~/.emacs.d/eshell"))

;; helm
;;
(after helm-autoloads
  (require 'helm-config))

;;
;; ido
(require 'ido)
(setq ido-enable-flex-matching t
      ido-ignore-buffers '("\\` " "^\*Mess" "^\*Back" ".*Completion" "^\*Ido")
      ido-work-directory-list '("~/" "~/Dropbox" "~/Documents")
      ido-case-fold t
      ido-use-filename-at-point nil
      ido-use-url-at-point nil
      ido-max-prospects 6
      ido-confirm-unique-completion t)
(ido-mode 1)

(after ido-hacks-autoloads
  (require 'ido-hacks)
  (ido-hacks-mode))

;; Window configuration stuff
(winner-mode 1)

;; Speedbar
(after sr-speedbar-autoloads
  (require 'sr-speedbar)
  (setq speedbar-select-frame-method 'attached)
  (setq speedbar-use-images t))

;; Ansi-term
(setq ansi-term-color-vector [unspecified "black" "red3" "green3" "yellow3" "DeepSkyBlue3" "magenta3" "cyan3" "white"])

;; Diredful - colored filenames for dired
;(require 'diredful)

(require 'dired-x)

;; Folding outline mode
(eval-after-load "outline" '(require 'foldout))

;; Tramp
(setq tramp-default-method "ssh")

(use-package projectile
  :diminish (projectile-mode . "Prj")
  :init (progn
          (projectile-global-mode)
          (setq projectile-completion-system 'grizzl)
          (add-to-list 'projectile-globally-ignored-directories ".svn")))

;;;; Editing
;;
;;
(setq next-line-add-newlines nil)
(transient-mark-mode t)
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)
(setq visual-line-fringe-indicators '(nil right-curly-arrow))

;; Gnuplot
(after gnuplot-autoloads
  (add-to-list 'auto-mode-alist '("\\.gp$" . gnuplot-mode)))

;; YAML
(after yaml-mode-autoloads
  (add-hook 'yaml-mode-hook
            '(lambda ()
               (define-key yaml-mode-map "\C-m" 'newline-and-indent))))


;;;; Programming
;;
;;
;;
;; Default code formatting
;;
(setq-default indent-tabs-mode nil)
(setq default-tab-width 2)
(setq-default c-basic-offset 2)

;;
;; Tags
;;

;; etags-table: search for a TAGS file upwards from current file.
;; etags-select: select from multiple occurrences of the same tag.
;;
;; (setq etags-table-alist ...)
(after etags-table-autoloads
  (require 'etags-table)
  (setq etags-table-search-up-depth 6))

(after etags-select-autoloads
  (require 'etags-select))

;;
;; Paredit and other lispiness
;;
(show-paren-mode +1)

; Much of this cribbed from http://bitbucket.org/eeeickythump/icky-dotemacs

(defun he-tag-beg ()
  (let ((p
         (save-excursion 
           (backward-word 1)
           (point))))
    p))

(defun tags-complete-tag (string predicate what)
  (when (boundp 'tags-completion-table)
    (save-excursion
      ;; If we need to ask for the tag table, allow that.
      (if (eq what t)
          (all-completions string tags-completion-table predicate)
        (try-completion string tags-completion-table
                        predicate)))))

(defun try-expand-tag (old)
  (unless  old
    (he-init-string (he-tag-beg) (point))
    (setq he-expand-list (sort
                          (all-completions he-search-string 'tags-complete-tag) 'string-lessp)))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
              (setq he-expand-list (cdr he-expand-list)))
  (if (null he-expand-list)
      (progn
        (when old (he-reset-string))
        ())
    (he-substitute-string (car he-expand-list))
    (setq he-expand-list (cdr he-expand-list))
    t))

(defun hippie-slime-symbol-beg ()
  (let ((p
	 (slime-symbol-start-pos)))
    p))

(defun try-expand-slime-symbol (old)
  (when (slime-connected-p)
    (unless  old
      (he-init-string (hippie-slime-symbol-beg) (point))
      (setq he-expand-list (sort
			    (car (slime-simple-completions
				  (buffer-substring-no-properties
				   (slime-symbol-start-pos)
				   (slime-symbol-end-pos))))
			    'string-lessp)))
    (while (and he-expand-list
		(he-string-member (car he-expand-list) he-tried-table))
      (setq he-expand-list (cdr he-expand-list)))
    (if (null he-expand-list)
	(progn
	  (when old (he-reset-string))
	  ())
      (he-substitute-string (car he-expand-list))
      (setq he-expand-list (cdr he-expand-list))
      t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hippie expand.  Groovy vans with tie-dyes.

;; Change the default hippie-expand order and add yasnippet to the front.
(setq hippie-expand-try-functions-list
      '(yas-hippie-try-expand
        try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-file-name
        try-complete-lisp-symbol))

;; Helps when debugging which try-function expanded
(setq hippie-expand-verbose t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Smart Tab

(defvar smart-tab-using-hippie-expand t
  "turn this on if you want to use hippie-expand completion.")

(defun smart-tab (prefix)
  "Needs `transient-mark-mode' to be on. This smart tab is
  minibuffer compliant: it acts as usual in the minibuffer.

  In all other buffers: if PREFIX is \\[universal-argument], calls
  `smart-indent'. Else if point is at the end of a symbol,
  expands it. Else calls `smart-indent'."
  (interactive "P")
  (labels ((smart-tab-must-expand (&optional prefix)
                                  (unless (or (consp prefix)
                                              mark-active)
                                    (looking-at "\\_>"))))
    (cond ((minibufferp)
           (minibuffer-complete))
          ((smart-tab-must-expand prefix)
           (if smart-tab-using-hippie-expand
               (hippie-expand prefix)
             (dabbrev-expand prefix)))
          ((smart-indent)))))

(defun smart-indent ()
  "Indents region if mark is active, or current line otherwise."
  (interactive)
  (if mark-active
    (indent-region (region-beginning)
                   (region-end))
    (indent-for-tab-command)))

;; Bind tab everywhere
;(global-set-key (kbd "TAB") 'smart-tab)

;; Enables tab completion in the `eval-expression` minibuffer
;(define-key read-expression-map [(tab)] 'hippie-expand)
;(define-key read-expression-map [(shift tab)] 'unexpand)

;;
;; Hideshow (also see individual language sections for hooks)
;;
(require 'hideshow)
;;(require 'hideshow-org)

(dolist (hook (list 'emacs-lisp-mode-hook
                    'c++-mode-hook
                    'ruby-mode-hook
                    'scheme-mode-hook
                    'java-mode-hook
                    'lisp-mode-hook
                    'perl-mode-hook
                    'sh-mode-hook
                    'php-mode-hook))
  (add-hook hook 'hs-minor-mode)
  ;;(add-hook hook 'hs-org/minor-mode)
  )

(define-fringe-bitmap 'hs-marker [0 24 24 126 126 24 24 0])

(defcustom hs-fringe-face 'hs-fringe-face
  "*Specify face used to highlight the fringe on hidden regions."
  :type 'face
  :group 'hideshow)

(defface hs-fringe-face
  '((t (:foreground "#888" :box (:line-width 2 :color "grey75" :style released-button))))
  "Face used to highlight the fringe on folded regions"
  :group 'hideshow)

(defcustom hs-face 'hs-face
  "*Specify the face to to use for the hidden region indicator"
  :type 'face
  :group 'hideshow)

(defface hs-face
  '((t (:background "#666" :box t)))
  "Face to hightlight the ... area of hidden regions"
  :group 'hideshow)

(defun display-code-line-counts (ov)
  (when (eq 'code (overlay-get ov 'hs))
    (let* ((marker-string "*fringe-dummy*")
           (marker-length (length marker-string))
           (display-string (format "(%d)..." (count-lines (overlay-start ov) (overlay-end ov))))
           )
      (overlay-put ov 'help-echo "Hiddent text. C-c,= to show")
      (put-text-property 0 marker-length 'display (list 'left-fringe 'hs-marker 'hs-fringe-face) marker-string)
      (overlay-put ov 'before-string marker-string)
      (put-text-property 0 (length display-string) 'face 'hs-face display-string)
      (overlay-put ov 'display display-string)
      )))

(setq hs-set-up-overlay 'display-code-line-counts)

(defun toggle-selective-display (column)
      (interactive "P")
      (set-selective-display
       (or column
           (unless selective-display
             (1+ (current-column))))))

(defun toggle-hiding (column)
      (interactive "P")
      (if hs-minor-mode
          (if (condition-case nil
                  (hs-toggle-hiding)
                (error t))
              (hs-show-all))
        (toggle-selective-display column)))

;;
;; ECB/CEDET
;;
;; For the time being, if we are on emacs < 23.2, just don't include this
;; at all, so I don't have to maintain a separate copy of cedet.

(when (or (>= emacs-major-version 24)
          (and (>= emacs-major-version 23)
               (>= emacs-minor-version 2)))
  
  (setq semanticdb-default-save-directory (expand-file-name "~/.semanticdb"))
  (unless (file-directory-p semanticdb-default-save-directory)
    (make-directory semanticdb-default-save-directory))

; Speedbar fix to use the one in cedet always

;(setq load-path (cons (concat (directory-of-library "cedet") "/../speedbar")
;                      load-path))
;(load-file (expand-file-name "~/.emacs.d/vendor/cedet/common/cedet.el"))

  (require 'cedet)
  (global-ede-mode 0)
  (require 'semantic/ia)
  (semantic-mode 1)

  (setq stack-trace-on-error t)
  ;; (require 'ecb)

  ;; (setq ecb-compilation-buffer-names
  ;;       (append ecb-compilation-buffer-names
  ;;               '(("\\(development\\|test\\|production\\).log" . t)
  ;;                 ("\\*R" . t))))

  (add-hook 'ecb-activate-hook
            (lambda () (setq global-semantic-idle-scheduler-mode nil))))

;;
;; ** Language specific inits go below here
;;

;;
;; Ruby
;;
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Rakefile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . ruby-mode))

;;
;; Shell
;;
(add-to-list 'auto-mode-alist '("\\.zsh$" . sh-mode))

;; XML/HTML
;;
(use-package web-mode
  :mode "\\.html$")

; (add-to-list 'auto-mode-alist
; (cons (concat "\\." (regexp-opt '("xml" "xsd" "sch" "rng" "xslt" "svg" "rss" "mako") t) "\\'")
;     'nxml-mode))
(unify-8859-on-decoding-mode)
;(setq magic-mode-alist (cons '("<\\?xml " . nxml-mode) magic-mode-alist))
;(fset 'xml-mode 'nxml-mode)
;(fset 'html-mode 'nxml-mode)
(add-hook 'nxml-mode-hook (lambda ()
                            (make-variable-buffer-local 'ido-use-filename-at-point)
                            (setq ido-use-filename-at-point nil)))


;; Scheme - cluck
(when (file-directory-p "~/src/cluck")
  (add-to-list 'load-path "~/src/cluck")
  (require 'cluck)
  (setq cluck-pretty-lambda-p nil))

;; Scheme - Geiser
(after geiser-autoloads
  (setq geiser-active-implementations '(racket)))

(when (file-exists-p "~/.emacs.d/vendor/quack.el")
  (setq quack-global-menu-p nil)
  (require 'quack))

;;;; Network
;;
;;
;;
;; Gnus
;;
(setq gnus-init-file "~/.emacs.d/dotgnus.el")

;;
;; IRC.  I really should choose a single client and delete the
;; others, shouldn't I?
;;
;; (require 'tls)
;; (defalias 'open-ssl-stream 'open-tls-stream)
(require 'ssl)
(require 'auth-source)

; Configuration for erc

;
;  Look up ERC passwords in .authinfo.gpg instead of having them
;  in this init file.
;
(defun n9-erc-ssl-with-authinfo (server port)
  (let ((n9-erc-auth (auth-source-user-or-password '("login" "password")
                                                   server
                                                   (number-to-string port))))
    (when n9-erc-auth
      (erc-ssl :server server
               :port port
               :nick (car n9-erc-auth)
               :password (cadr n9-erc-auth)))))

(defun erc-freenode ()
  (interactive)
  (n9-erc-ssl-with-authinfo "freenode.the-gadgeteer.com" 57002))

(defun erc-bitlbee ()
  (interactive)
  (n9-erc-ssl-with-authinfo "bitlbee.the-gadgeteer.com" 57002))

(setq erc-max-buffer-size 20000)
(defvar erc-insert-post-hook)
(add-hook 'erc-insert-post-hook 'erc-truncate-buffer)
(setq erc-truncate-buffer-on-save t)


;;;; Org-mode
;;
;;

(use-package dotorg)

;; Magic
;;
;; (org-clock-persistence-insinuate)

(after org-journal-autoloads
  (require 'org-journal)
  (setq org-journal-date-format "%Y-%m-%d %A")
  (setq org-journal-dir "~/Dropbox/journal/"))

(defun n9-replace-all-in-buffer (srch repl)
  (save-excursion
    (goto-char (point-min))
    (while (search-forward srch nil t)
      (replace-match repl))))

(defun n9-fix-scene-breaks-in-latex ()
  (cond ((string= org-export-latex-class "sffms")
         (n9-replace-all-in-buffer "\\hrule" "\\\\newscene"))
        ((string= org-export-latex-class "memoir")
         (n9-replace-all-in-buffer "\\hrule" "\\\\pfbreak"))))
    
(defun unfill-paragraph ()
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))

(defun unfill-region (start end)
  (interactive "r")
  (let ((fill-column (point-max)))
    (fill-region start end nil)))
;;
;; A clever little link hack from worg
;; Make a link like [[latex:foo][bar]] and it gets turned into
;; \foo{bar}...




;; IImage
(require 'iimage)
(add-to-list 'iimage-mode-image-regex-alist
             (cons (concat "\\[\\[file:\\(~?" iimage-mode-image-filename-regex
                           "\\)\\]")  1))

(defun org-toggle-iimage-in-org ()
  "display images in your org file"
  (interactive)
  (if (face-underline-p 'org-link)
      (set-face-underline-p 'org-link nil)
      (set-face-underline-p 'org-link t))
  (iimage-mode 1))


(after diminish-autoloads
  (require 'diminish))

;;;; Key Bindings
;;
;;

;(global-set-key "\C-ce" 'ecb-minor-mode)
(global-set-key "\C-cg" 'magit-status)
(global-set-key "\C-cs" 'eshell)
(global-set-key "\C-ct" 'multi-term-dedicated-toggle)

(global-set-key "\C-x\C-b" 'ibuffer)
(global-set-key "\C-m" 'newline-and-indent)

(global-set-key [?\C-`] 'sr-speedbar-toggle)
(global-set-key [?\M-/] 'auto-complete)

;(global-set-key "\M-?" 'etags-select-find-tag-at-point)
;(global-set-key "\M-." 'etags-select-find-tag)

;;
;; Meta isn't that inconvenient on my usual keyboard,
;; but just in case, this is better than escape...
;;
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)


;; Finally, start the server
(load-library "server")
(unless (server-running-p) (server-start))

;(message "My .emacs loaded in %ds" (destructuring-bind (hi lo ms) (current-time)
;                             (- (+ hi lo) (+ (first *emacs-load-start*) (second
;                             *emacs-load-start*)))))

(message "Done loading .emacs")
