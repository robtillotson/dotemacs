(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 151 t)
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(custom-file "~/.emacs.d/custom.el")
 '(custom-safe-themes
   (quote
    ("41c926d688a69c7d3c7d2eeb54b2ea3c32c49c058004483f646c1d7d1f7bf6ac" "fe704e7c447c85c0cd971713a854cbbd59241ce7190e7c4bbaa58ffeb7107eb6" "5bcd0c26bad3303c0325d12dd6562e4f7892d39d390d7db194dd141ba971cad7" "61f7aca39aa3d5cadc9d64a9ee19a4a0e04821cb4314670653aaae600dc24b19" "cc0dbb53a10215b696d391a90de635ba1699072745bf653b53774706999208e3" "1fab355c4c92964546ab511838e3f9f5437f4e68d9d1d073ab8e36e51b26ca6a" "0c311fb22e6197daba9123f43da98f273d2bfaeeaeb653007ad1ee77f0003037" "9b402e9e8f62024b2e7f516465b63a4927028a7055392290600b776e4a5b9905" "01d8c9140c20e459dcc18addb6faebd7803f7d6c46d626c7966d3f18284c4502" "83279c1d867646c5eea8a804a67a23e581b9b3b67f007e7831279ed3a4de9466" "3539b3cc5cbba41609117830a79f71309a89782f23c740d4a5b569935f9b7726" "d72836155cd3b3e52fd86a9164120d597cbe12a67609ab90effa54710b2ac53b" "40bc0ac47a9bd5b8db7304f8ef628d71e2798135935eb450483db0dbbfff8b11" "603a9c7f3ca3253cb68584cb26c408afcf4e674d7db86badcfe649dd3c538656" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "1b8d67b43ff1723960eb5e0cba512a2c7a2ad544ddb2533a90101fd1852b426e" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "cea6d15a8333e0c78e1e15a0524000de69aac2afa7bb6cf9d043a2627327844e" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" default)))
 '(default-frame-alist nil)
 '(ecb-excluded-directories-regexps (quote ("^\\(CVS\\|\\.[^xX]*\\)$")))
 '(ecb-layout-name "left15")
 '(ecb-options-version "2.50")
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--mouse-2))
 '(ecb-source-file-regexps
   (quote
    ((".*"
      ("\\(^\\(\\.\\|#\\)\\|\\(~$\\|\\.\\(elc\\|obj\\|o\\|class\\|lib\\|dll\\|a\\|so\\|cache\\|pyc\\|pyo\\)$\\)\\)")
      ("^\\.\\(emacs\\|gnus\\)$")))))
 '(ecb-source-path
   (quote
    (("~/jx/s/trunk" "jx trunk")
     ("~/jx/s/89m" "jx 89m")
     ("~/jx/s" "jx sandboxes")
     "~/Dropbox"
     ("/" "/")
     ("~/.emacs.d/" "Emacs Config")
     ("~/.zsh.d/" "ZSH Config"))))
 '(ecb-tip-of-the-day nil)
 '(ecb-use-speedbar-instead-native-tree-buffer (quote dir))
 '(ecb-windows-width 0.2)
 '(make-backup-files nil)
 '(mumamo-chunk-coloring 1)
 '(org-agenda-files
   (quote
    ("~/Dropbox/Notes/gaming.org" "~/Dropbox/Notes/media.org" "~/Dropbox/Notes/agenda.org" "~/Dropbox/Notes/jx.org")))
 '(package-selected-packages
   (quote
    (realgud org-board tide indium js2-mode evil-magit pocket-reader org-wc evil-paredit swiper ivy counsel-dash counsel-projectile ivy-hydra ivy-rich counsel smex xml-rpc json-rpc org-bullets nov origami dumb-jump zerodark-theme all-the-icons org-super-agenda outshine emacsql emacsql-sqlite ox-twbs helm treemacs treemacs-evil indent-guide docker-api dockerfile-mode ob-restclient restclient ein ob-kotlin ob-ipython ox-rst htmlize ox-reveal evil-org helm-dash zeal-at-point defproject project-explorer persp-projectile projectile paredit-menu paredit ecb etags-table dsvn git-annex magit-annex magit ac-cider cider web-mode rinari anaconda-mode pyenv-mode-auto pyenv-mode ensime lentic yaml-mode fountain-mode markdown-mode multiple-cursors fold-dwim docker dired-k sr-speedbar helm-flycheck helm-descbinds bookmark+ deft auto-complete multi-term yasnippet diff-hl which-key evil-god-state god-mode evil-commentary evil-surround evil-indent-plus evil-leader evil ace-isearch ace-window expand-region undo-tree paradox hydra perspective fullframe popwin focus writeroom-mode powerline color-theme f dash use-package)))
 '(paradox-automatically-star nil)
 '(paradox-github-token t)
 '(paren-match-face (quote paren-face-match-light))
 '(paren-sexp-mode t)
 '(safe-local-variable-values
   (quote
    ((eval auto-fill-mode -1)
     (py-indent-offset . 4)
     (ruby-compilation-executable . "ruby")
     (ruby-compilation-executable . "ruby1.8")
     (ruby-compilation-executable . "ruby1.9")
     (ruby-compilation-executable . "rbx")
     (ruby-compilation-executable . "jruby"))))
 '(select-enable-clipboard t)
 '(size-indication-mode t)
 '(viper-toggle-key "z"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mumamo-background-chunk-submode2 ((((class color) (min-colors 88) (background dark)) (:background "#202020"))) t)
 '(mumamo-background-chunk-submode3 ((((class color) (min-colors 88) (background dark)) (:background "#282828"))) t)
 '(mumamo-background-chunk-submode4 ((((class color) (min-colors 88) (background dark)) (:background "#282828"))) t))
