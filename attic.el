
;; While "after" is the basic eval-after-load macro, the following
;; macros are syntactic sugar to make package configuration a little
;; more declarative.
;;
;; (require-package foo) - if foo is installed, "require"s it and executes body after load.
;; (with-package foo) - executes body after package is installed.
;; (when-package-installed foo) - executes body only if package is installed
;;
;; If I ever decide not to use pallet/carton to manage packages, these could be
;; augmented to automatically select packages for installation etc.

(defmacro require-package (sym &rest body)
  (declare (indent 1) (debug t))
  (let ((autoloads (intern (concat (symbol-name sym) "-autoloads"))))
    `(progn
       (eval-after-load ',autoloads
         '(require ',sym))
       (eval-after-load ',sym
         '(progn ,@body)))))

(defmacro with-package (package &rest body)
  (declare (indent 1) (debug t))
  `(eval-after-load ',package
     '(progn ,@body)))

;; A version of "after" that makes a little clearer what
;; loading after foo-autoloads actually means.
(defmacro when-package-installed (pkg &rest body)
  (declare (indent 1) (debug t))
  (let ((autoloads (intern (concat (symbol-name sym) "-autoloads"))))
    `(eval-after-load ',autoloads
       '(progn ,@body))))


;; Like with-dev-package but immediately requires the package too.
(defmacro with-dev-package* (sym &rest body)
  (declare (indent 1) (debug t))
  `(progn
     (with-dev-package ,sym)
     (require ',sym)))

(defun rob/find-installed-cask-package ()
  "Locate a currently installed version of cask.el, from the cask-installed elpa dir."
  (let* ((cask-elpa-path (format "%s/.cask/%s/elpa" user-emacs-directory emacs-version))
         (cask-paths 
          (delq nil (mapcar (lambda (x) (and (> (length x) 5)
                                             (string= "cask-" (substring x 0 5))
                                             x))
                            (directory-files cask-elpa-path)))))
    (when cask-paths
      (expand-file-name (format "%s/%s/cask.el" cask-elpa-path (car cask-paths))))
    ))

;(require 'cask (rob/find-installed-cask-package))

; Much of this cribbed from http://bitbucket.org/eeeickythump/icky-dotemacs

(defun he-tag-beg ()
  (let ((p
         (save-excursion 
           (backward-word 1)
           (point))))
    p))

(defun tags-complete-tag (string predicate what)
  (when (boundp 'tags-completion-table)
    (save-excursion
      ;; If we need to ask for the tag table, allow that.
      (if (eq what t)
          (all-completions string tags-completion-table predicate)
        (try-completion string tags-completion-table
                        predicate)))))

(defun try-expand-tag (old)
  (unless  old
    (he-init-string (he-tag-beg) (point))
    (setq he-expand-list (sort
                          (all-completions he-search-string 'tags-complete-tag) 'string-lessp)))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
              (setq he-expand-list (cdr he-expand-list)))
  (if (null he-expand-list)
      (progn
        (when old (he-reset-string))
        ())
    (he-substitute-string (car he-expand-list))
    (setq he-expand-list (cdr he-expand-list))
    t))

(defun hippie-slime-symbol-beg ()
  (let ((p
	 (slime-symbol-start-pos)))
    p))

(defun try-expand-slime-symbol (old)
  (when (slime-connected-p)
    (unless  old
      (he-init-string (hippie-slime-symbol-beg) (point))
      (setq he-expand-list (sort
			    (car (slime-simple-completions
				  (buffer-substring-no-properties
				   (slime-symbol-start-pos)
				   (slime-symbol-end-pos))))
			    'string-lessp)))
    (while (and he-expand-list
		(he-string-member (car he-expand-list) he-tried-table))
      (setq he-expand-list (cdr he-expand-list)))
    (if (null he-expand-list)
	(progn
	  (when old (he-reset-string))
	  ())
      (he-substitute-string (car he-expand-list))
      (setq he-expand-list (cdr he-expand-list))
      t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hippie expand.  Groovy vans with tie-dyes.

;; Change the default hippie-expand order and add yasnippet to the front.
(setq hippie-expand-try-functions-list
      '(yas-hippie-try-expand
        try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-file-name
        try-complete-lisp-symbol))

;; Helps when debugging which try-function expanded
(setq hippie-expand-verbose t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Smart Tab

(defvar smart-tab-using-hippie-expand t
  "turn this on if you want to use hippie-expand completion.")

(defun smart-tab (prefix)
  "Needs `transient-mark-mode' to be on. This smart tab is
  minibuffer compliant: it acts as usual in the minibuffer.

  In all other buffers: if PREFIX is \\[universal-argument], calls
  `smart-indent'. Else if point is at the end of a symbol,
  expands it. Else calls `smart-indent'."
  (interactive "P")
  (labels ((smart-tab-must-expand (&optional prefix)
                                  (unless (or (consp prefix)
                                              mark-active)
                                    (looking-at "\\_>"))))
    (cond ((minibufferp)
           (minibuffer-complete))
          ((smart-tab-must-expand prefix)
           (if smart-tab-using-hippie-expand
               (hippie-expand prefix)
             (dabbrev-expand prefix)))
          ((smart-indent)))))

(defun smart-indent ()
  "Indents region if mark is active, or current line otherwise."
  (interactive)
  (if mark-active
    (indent-region (region-beginning)
                   (region-end))
    (indent-for-tab-command)))

;; Bind tab everywhere
;(global-set-key (kbd "TAB") 'smart-tab)

;; Enables tab completion in the `eval-expression` minibuffer
;(define-key read-expression-map [(tab)] 'hippie-expand)
;(define-key read-expression-map [(shift tab)] 'unexpand)

(after smart-mode-line-autoloads
  (setq evil-mode-line-format 'before)
  (setq evil-normal-state-tag   "  N  "
        evil-emacs-state-tag    (propertize "  Em  " 'face '((:foreground "orange")))
        evil-insert-state-tag   (propertize "  Ins  " 'face '((:foreground "grey80")))
        evil-motion-state-tag   (propertize "  Mot  " 'face '((:foreground "blue")))
        evil-visual-state-tag   (propertize "  Vis  " 'face '((:foreground "red")))
        evil-operator-state-tag (propertize "  Op  " 'face '((:foreground "purple"))))
  (setq sml/vc-mode-show-backend t)

  (setq sml/replacer-regexp-list
        '(("^~/Dropbox/" ":DB:")
          ("^~/\\.emacs\\.d/" ":Emacs:")
          ("^~/jx/s/\\(.*?\\)/wtp/wtlib/jx_api/" ":jxAPI/\\1:")
          ("^~/jx/s/\\(.*?\\)/wtp/\\(.*?\\)/" ":JX/\\1/\\2:")
          ("^~/jx/s/\\(.*?\\)/" ":JX/\\1:")
          ))

  (add-hook 'after-init-hook 'sml/setup))

;; Declare a development package that we are working on ourselves.
;; If it has a source directory in ~/src/... add that to the load
;; path and immediately execute the body.  Otherwise, if the
;; package is installed execute the body on an eval-after-load of
;; the autoloads.
(defmacro with-dev-package (sym &rest body)
  (declare (indent 1) (debug t))
  (let ((path (expand-file-name (concat "~/src/" (symbol-name sym)))))
    `(cond ((file-directory-p ,path)
            (add-to-list 'load-path ,path)
            (progn ,@body))
           ((package-installed-p ',sym)
            (eval-after-load (symbol-name ',sym)
              '(progn ,@body))))))
