
;; Color theme based on ir_black for vim

(defvar irblack-normal "#f6f3e8")
(defvar irblack-string "#a8ff60")
(defvar irblack-string-inner "#00a0a0")
(defvar irblack-number "#ff73fd")
(defvar irblack-comment "#7c7c7c")
(defvar irblack-keyword "#96cbfe")
(defvar irblack-operator "#ffffff")
(defvar irblack-class "#ffffb6")
(defvar irblack-method "#ffd2a7")
(defvar irblack-regexp "#e9c062")
(defvar irblack-regexp-alt "#ff8000")
(defvar irblack-regexp-alt2 "#b18a3d")
(defvar irblack-variable "#c6c5fe")

(defvar irblack-red "#ff6c60")
(defvar irblack-lightred "#ffb6b0")
(defvar irblack-brown "#e18964")
(defvar irblack-lightpurple "#ffccff")

(require 'color-theme)

(defun color-theme-irblack ()
  "Color theme based on ir_black."
  (interactive)
  (color-theme-install
   '(color-theme-irblack
     ((foreground-color . "#f6f3e8")
      (background-color . "#000000")
      (cursor-color . "#ffa560")
      (background-mode . dark))
     (region ((t (:background "#2d2e3c"))))
     (highlight ((t (:background "#2d2e3c"))))
     (show-paren-match-face ((t (:background "#857b6f"))))
     (modeline ((t (:bold t :background "#c0c0c0" :foreground "#000000"))))
     (modeline-inactive ((t (:background "#606060" :foreground "#000000"))))
     (font-lock-builtin-face ((t (:foreground "#6699cc"))))
     (font-lock-comment-face ((t (:foreground "#7c7c7c"))))
     (font-lock-constant-face ((t (:foreground "#ff73fd"))))
     (font-lock-doc-face ((t (:foreground "#a8ff60"))))
     (font-lock-function-name-face ((t (:foreground "#ffd2a7"))))
     (font-lock-keyword-face ((t (:foreground "#96cbfe"))))
     (font-lock-preprocessor-face ((t (:foreground "#96cbfe"))))
     (font-lock-reference-face ((t (:foreground "#ffccff"))))
     (font-lock-string-face ((t (:foreground "#a8ff60"))))
     (font-lock-type-face ((t (:foreground "#ffffb6"))))
     (font-lock-variable-name-face ((t (:foreground "#c6c5fe")))))))
