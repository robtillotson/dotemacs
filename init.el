;;; init.el -- rob's emacs init

;;; Commentary:
;; Simple Emacs init where most stuff is in a tangled org file.

;;; Code:

;(require 'cask "~/.cask/cask.el")
;(cask-initialize)
(require 'package)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)
(when (eq system-type 'windows-nt)
  (add-to-list 'exec-path "c:/Program Files/Git//bin")
  (add-to-list 'exec-path "c:/Program Files/Git/usr/bin"))
(require 'ob-tangle)
(org-babel-load-file "~/.emacs.d/emacs-init.org")
