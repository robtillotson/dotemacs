
(require 'org)
(require 'org-table)
(require 'parse-time)
(require 'time-date)

(defvar org-nanowrimo-start-date "2015-11-01"
  "The starting date of your NaNoWriMo style writing challenge, in YYYY-MM-DD form.

Consider making this buffer-local.")

(defvar org-nanowrimo-end-date "2015-11-30"
  "The ending date of your NaNoWriMo style writing challenge, in YYYY-MM-DD form.

Consider making this buffer-local.")

(defvar org-nanowrimo-target-words 50000
  "The number of words to write in the challenge period.")

(defun org-nanowrimo-setup (words start end)
  "Set up local variables for a NaNoWriMo style challenge.

START and END are the first and last days of the challenge in YYYY-MM-DD format.
WORDS is the number of words."
  (setq-local org-nanowrimo-target-words words)
  (setq-local org-nanowrimo-start-date start)
  (setq-local org-nanowrimo-end-date end))

(defun day-to-time (day)
  (encode-time 0 0 0 day 1 1))

(defun org--date-to-day (date)
  (time-to-days (apply 'encode-time (org-parse-time-string date))))

;; Why the extra layers of conversion using day numbers instead of just using
;; times?  Because if you just add 1 day to a time, it chokes on DST which
;; just happens to change over during November...
(defun org--nanowrimo-insert-rows (day endday)
  (when (<= day endday)
    (insert "| # | ")
    (insert (format-time-string "[%Y-%m-%d %a]" (day-to-time day)))
    (insert " | | | | | | |\n")
    (org--nanowrimo-insert-rows (+ day 1) endday)
    ))

(defun org-nanowrimo-insert-progress-table ()
  "Insert a NaNoWriMo progress table in the current org buffer.

The progress table is set up to approximate the progress spreadsheets that are
commonly used during NaNo, including calculations of pace and word targets.
Use it by filling in your daily word count in the appropriate column, then
use C-c C-c to recalculate it."
  (interactive)
  (let ((separator "|---+------------------+--------+-------+-----------+-------------+------------+------------|\n")
        (start-day (org--date-to-day org-nanowrimo-start-date))
        (end-day (org--date-to-day org-nanowrimo-end-date))
        (pos (point)))
    (save-excursion
      (insert "#+tblname: nano_progress\n")
      (insert separator)
      (insert "| _ | EndDate          | Target |   Avg |  DaysLeft |       Count |    NewPace |            |\n")
      (insert "| * | ")
      ;; timestamp
      (insert (format-time-string "[%Y-%m-%d %a]" (day-to-time end-day)))
      (insert " | " (number-to-string org-nanowrimo-target-words))
      (insert " |  |  |  |  |  |\n")
      (insert separator)
      (insert "|   | Date             | Words  | Today | Remaining | Orig Target | Difference | New Target |\n")
      (insert separator)
      (org--nanowrimo-insert-rows start-day end-day)
      (insert separator)
      (insert "#+TBLFM: $4=if($3>0,$3-(@-1$3),0) ;N%d::$5=if($3>0,$Target-$3,0);N%d::$6=($Target/30)*(30-(<$EndDate>-<$2>));%d::$7=if($3>0,$3-$6,0);N%d::$8=if($3>0,0,((($Target-$Count)/$DaysLeft)*($DaysLeft-(<$EndDate>-<$2>)))+$Count;%d::$Avg=$Count/vlen(@+II$2..@+III$2);%d::$Count=vmax(@+II$3..@+III$3);%d::$DaysLeft=vlen(@+II$2..@+III$2)-vlen(@+II$3..@+III$3);%d::$NewPace=($Target-$Count)/$DaysLeft;%d")
      (goto-char pos)
      (forward-line 2)
      (org-table-align)
      )))
  
(defun org--nanowrimo-update-progress-table-for-day (day wordcount)
  (let ((timestamp (format-time-string "[%Y-%m-%d %a]" (day-to-time day))))
    (save-excursion
      (goto-char (point-min))
      (re-search-forward (org-babel-named-data-regexp-for-name "nano_progress"))
      (re-search-forward (format "^\\s *| +# +| +%s +|" (regexp-quote timestamp)))
      (org-table-get-field 3 (format "%s" wordcount))
      (org-table-recalculate t nil))))

(defun org-nanowrimo-update-progress-table (&optional date)
  (interactive)
  (let* ((start-day (org--date-to-day org-nanowrimo-start-date))
         (end-day (org--date-to-day org-nanowrimo-end-date))
         (today (if date
                    (org--date-to-day date)
                  (time-to-days (current-time))))
         (wordcount (org-word-count-aux (point-min) (point-max)))
         (percentdone (* (/ (float wordcount) org-nanowrimo-target-words) 100))
         (wcmessage (format "%s words out of %s (%.1d%%)" wordcount org-nanowrimo-target-words percentdone)))
    (cond
     ((< today start-day) (message "Your writing challenge starts in %s days, and you already have %s" (- start-day today) wcmessage))
     ((> today end-day) (message "Your writing challenge is over, but you now have %s" wcmessage))
     (t (org--nanowrimo-update-progress-table-for-day today wordcount)
        (message "You have written %s" wcmessage)))))

(provide 'org-nanowrimo)
  
