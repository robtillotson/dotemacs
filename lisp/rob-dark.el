;;
;; rob@pyrite.org - personal color theme for emacs
;;
(require 'color-theme)

(defun rob-dark ()
  (interactive)
  (color-theme-install
   '(rob-dark
      ((background-color . "#000000")
       (background-mode . dark)
       (border-color . "#1a1a1a")
       (cursor-color . "#fce94f")
       (foreground-color . "#ebebeb")
       (mouse-color . "black"))
      ;;; some basic colors we can inherit from
      (robdark-blue ((t (:foreground "#b0d7e8"))))
      (robdark-blue-1 ((t (:foreground "#a1c3e8"))))
      (robdark-blue-2 ((t (:foreground "#92b3e8"))))
      (robdark-gray ((t (:foreground "#878787"))))
      (robdark-purple ((t (:foreground "#ff73fd"))))
      (robdark-purple-1 ((t (:foreground "#ffccff"))))
      (robdark-purple-2 ((t (:foreground "#d4caed"))))
      (robdark-green ((t (:foreground "#b0e3b5"))))
      (robdark-yellow ((t (:foreground "#e9d99f"))))
      (robdark-yellow-1 ((t (:foreground "#dae085"))))
      (robdark-red ((t (:foreground "#c42149"))))
      (robdark-magenta ((t (:foreground "#ff5996"))))
      (robdark-peach ((t (:foreground "#e5786d"))))
      
      (fringe ((t (:background "#1a1a1a"))))
      (region ((t (:background "#235251"))))
      (highlight ((t (:background "#235251"))))
      (show-paren-match-face ((t (:background "#857b6f"))))
      (mode-line ((t (:inherit 'variable-pitch
                               ;; :box (:line-width 1 :style pressed-button)
                               :background "#6183a8" :foreground "#ebebeb"
                               :weight normal))))
      (mode-line-inactive ((t (:inherit 'mode-line
                                        ;; :box (:line-width 1 :style pressed-button)
                                        :background "#303030" :foreground "#666666"
                                        :weight normal))))
      (mode-line-buffer-id ((t (:inherit 'mode-line :bold t :foreground "#000000"))))
      ;;
      ;; Powerline
      ;;
      (powerline-active1 ((t (:inherit mode-line :background "grey22" :foreground "#ebebeb"))))
      (powerline-active2 ((t (:inherit mode-line :background "grey40" :foreground "#ebebeb"))))
      (powerline-inactive1 ((t (:inherit mode-line-inactive :background "grey11" :foreground "#ebebeb"))))
      (powerline-inactive2 ((t (:inherit mode-line-inactive :background "grey20" :foreground "#ebebeb"))))
      ;;
      (tool-tips ((t (:inherit 'variable-pitch :foreground "black" :background "lightyellow"))))
      (tooltip ((t (:inherit 'variable-pitch :foreground "black" :background "lightyellow"))))
      (bold ((t (:bold t :underline nil :background nil))))
      (italic ((t (:italic t :underline nil :background nil))))

      (font-lock-builtin-face ((t (:inherit robdark-blue))))
      (font-lock-comment-face ((t (:inherit robdark-gray))))
      (font-lock-constant-face ((t (:inherit robdark-peach))))
      (font-lock-doc-face ((t (:inherit robdark-green))))
      (font-lock-function-name-face ((t (:inherit robdark-magenta))))
      (font-lock-keyword-face ((t (:inherit robdark-blue-1))))
      (font-lock-preprocessor-face ((t (:inherit robdark-blue-2)))) ;; keyword?
      (font-lock-reference-face ((t (:inherit robdark-purple-1))))
      (font-lock-string-face ((t (:inherit robdark-green))))
      (font-lock-type-face ((t (:inherit robdark-yellow))))
      (font-lock-variable-name-face ((t (:inherit robdark-purple-2))))
      (minibuffer-prompt ((t (:foreground "#cee0f3" :bold t))))
      (font-lock-warning-face ((t (:foreground "#c42149" :bold t))))
      ;; Workgroups
      (wg-mode-line-face ((t (:inherit 'variable-pitch :background "#b0d7e8" :foreground "#000000"))))
      ;; Cluck
      (cluck-pltish-comment-face ((t (:inherit font-lock-comment-face))))
      (cluck-pltish-paren-face ((t (:foreground "#f0f0f0" :bold t))))
      (cluck-pltish-selfeval-face ((t (:inherit font-lock-constant-face))))
      (cluck-pltish-colon-keyword-face ((t (:inherit font-lock-preprocessor-face))))
      (cluck-pltish-defn-face ((t (:inherit font-lock-function-name-face))))
      (cluck-pltish-module-defn-face ((t (:inherit font-lock-type-face))))
      (cluck-pltish-keyword-face ((t (:inherit font-lock-keyword-face))))
      (cluck-threesemi-semi-face ((t (:inherit font-lock-doc-face))))
      (cluck-threesemi-text-face ((t (:inherit font-lock-doc-face))))
      
      ;; Flymake et. al.
      (flymake-errline ((t (:underline "red"))))
      (flymake-warnline ((t (:underline "yellow"))))
      
      ;; Org-mode
      (org-hide ((t (:foreground "#000000"))))
      (org-level-1 ((t (:inherit robdark-yellow :height 1.0))))
      (org-level-2 ((t (:inherit robdark-purple-2 :height 1.0))))
      (org-level-3 ((t (:inherit robdark-blue :height 1.0))))
      (org-level-4 ((t (:inherit robdark-blue-1))))
      (org-level-5 ((t (:inherit robdark-green))))
      (org-level-6 ((t (:inherit robdark-peach))))
      (org-todo ((t (:bold t :foreground "#cc2626"))))
      (org-done ((t (:bold t :foreground "#66ee66"))))
      (org-block ((t (:foreground "#bbbbbc"))))
      (org-block-background ((t (:background "#101010"))))
      (org-quote ((t (:inherit org-block :slant italic))))
      (org-verse ((t (:inherit org-block :slant italic))))

      ;; Terminal
      (term-color-black ((t (:foreground "#000000"))))
      (term-color-red ((t (:inherit robdark-red))))
      (term-color-green ((t (:inherit robdark-green))))
      (term-color-yellow ((t (:inherit robdark-yellow))))
      (term-color-blue ((t (:inherit robdark-blue))))
      (term-color-magenta ((t (:inherit robdark-magenta))))
      (term-color-cyan ((t (:foreground "cyan3"))))
      (term-color-white ((t (:foreground "#ebebeb"))))
      (term-default-fg-color ((t (:inherit term-color-white))))
      (term-default-bg-color ((t (:inherit term-color-black))))
      ))
  )


(defun rob-dark-tty ()
  (interactive)
  (color-theme-install
   '(rob-dark-tty
     ((background-color . "black")
      (background-mode . dark)
      (foreground-color . "white")
      (mouse-color . "black")
      (cursor-color . "yellow"))
     (default ((t (nil))))
     (region ((t (:background "gray"))))
     (highlight ((t (:background "gray"))))
     (mode-line ((t (:background "white" :foreground "black"))))
     (bold ((t (:bold t :background "black" :foreground "white"))))
     (bold-italic ((t (:bold t :underline t :foreground "white"))))
     (font-lock-builtin-face ((t (:foreground "blue"))))
     (font-lock-comment-face ((t (:foreground "gray"))))
     (font-lock-constant-face ((t (:foreground "magenta"))))
     (font-lock-doc-face ((t (:foreground "green"))))
     (font-lock-function-name-face ((t (:foreground "magenta"))))
     (font-lock-keyword-face ((t (:foreground "cyan"))))
     (font-lock-preprocessor-face ((t (:foreground "cyan"))))
     (font-lock-reference-face ((t (:foreground "magenta"))))
     (font-lock-string-face ((t (:foreground "green"))))
     (font-lock-type-face ((t (:foreground "yellow"))))
     (font-lock-variable-name-face ((t (:foreground "magenta"))))
     (font-lock-warning-face ((t (:foreground "red"))))
     (cluck-pltish-comment-face ((t (:inherit font-lock-comment-face))))
     (cluck-pltish-paren-face ((t (:foreground "#f0f0f0" :bold t))))
     (cluck-pltish-selfeval-face ((t (:inherit font-lock-constant-face))))
     (cluck-pltish-colon-keyword-face ((t (:inherit font-lock-preprocessor-face))))
     (cluck-pltish-defn-face ((t (:inherit font-lock-function-name-face))))
     (cluck-pltish-module-defn-face ((t (:inherit font-lock-type-face))))
     (cluck-pltish-keyword-face ((t (:inherit font-lock-keyword-face))))
     (cluck-threesemi-semi-face ((t (:inherit font-lock-doc-face))))
     (cluck-threesemi-text-face ((t (:inherit font-lock-doc-face)))))))

(provide 'rob-dark)

