(require 'org)
(require 'ob)

(defun rob/org-map-log-tables (&optional func)
  (let ((regexp (concat org-babel-name-regexp (regexp-quote "logtbl_"))))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward regexp nil t)
        (forward-line)
        (when func (funcall func))
        ))))

(defun rob/org-table-insert-dated-row (&optional hlines-above)
  (let ((beg (org-table-begin))
        (end (org-table-end))
        (count (or hlines-above 2)))
    (goto-char end)
    (re-search-backward org-table-hline-regexp beg t count)
    (org-table-insert-row)
    (org-table-get-field 1 "#")
    (org-table-get-field 2 (format-time-string "[%Y-%m-%d %a]"))
    (org-table-align)
    ))


(defun rob/add-dated-row-to-log-tables ()
  (interactive)
  (rob/org-map-log-tables 'rob/org-table-insert-dated-row))

(provide 'org-personal-log)
